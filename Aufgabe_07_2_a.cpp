#include <iostream>
#include "Aufgabe_07_2_a.h"

using namespace std;


double* Punkt::getCoords() {
    return coords;
}

int Punkt::getDimensions() {
    return n;
}

void Punkt::setCoords(double* coords) {
    for (int i = 0; i < n; i++) {
        this->coords[i] = coords[i];
    }
}

void Punkt::setDimensions(int n) {
    this->n = n;
}

Punkt& Punkt::operator+(const Punkt& p) {
    if (n == p.n) {
        for (int i = 0; i < n; i++) {
            coords[i] += p.coords[i];
        }
    } else {
        // Undefined behaviour. I love it.
    }

    return *this;
}

void Punkt::scale(double d) {
    for (int i = 0; i <n; i++) {
        coords[i] *= d;
    }
}

double Punkt::dotProd(const Punkt& p) {
    double sum = 0.0;
    if (n == p.n) {
        for (int i = 0; i < n; i++) {
            sum += coords[i] * p.coords[i];
        }
    } else {
        // Mmh.. Smells like undefined behaviour.
    }

    return sum;
}