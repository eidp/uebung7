#include <iostream>
#include "Aufgabe_07_2_a.h"


using namespace std;


int main() {
    double coords1[] = {2.5, 3.8, -2.2};
    double coords2[] = {0.8, -1.2, 3.1};

    Punkt a(3);
    Punkt b(3);
    
    a.setCoords(coords1);
    b.setCoords(coords2);

    a = a + b;

    b.scale(3);

    cout << "[*] DotProd: " << a.dotProd(b) << endl;

    return 0;
}