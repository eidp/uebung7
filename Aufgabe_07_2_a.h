#ifndef _FCK_CPP_AND_ITS_RETARDED_HEADER_SHIT_PUNKT_
#define _FCK_CPP_AND_ITS_RETARDED_HEADER_SHIT_PUNKT_

class Punkt {
    double* coords;
    unsigned int n;

public:
    Punkt(unsigned int n) : n(n) {
        coords = new double[n];

        for (int i = 0; i < n; i++) {
            coords[i] = 0.0;
        }
    }
    ~Punkt() {
        delete[] coords;
    }

    double* getCoords();
    int getDimensions();
    void setCoords(double* coords);
    void setDimensions(int n);
    void scale(double d);
    double dotProd(const Punkt& p);

    // CAP'N MACHINES ARE OVERLOADING!!!
    Punkt& operator+(const Punkt& p);
};

#endif